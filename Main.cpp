#include <iostream>
#include "myList.h"
using namespace std;


void main() 
{
	myList<int> list;
	list.push_back(1);
	list.push_back(2);
	list.push_back(3);
	list.push_back(4);
	myList<int>::Iterator i = list.rbegin();
	do
	{
		cout << *i << endl;
		i--;
	} while (i != list.end());

	system("pause");
}