#pragma once
#include <iostream>
using namespace std;


template <typename T> class myList {
	struct Node
	{
		T val;
		Node *next, *prev;
	};

	int size;
	Node *head, *tail;

public:
	myList();
	myList(const myList &list);
	~myList();
	int get_size();
	void clear();
	bool is_empty();
	bool value_is_find(T val);
	T& at(int pos);
	bool change_value(int pos);
	int get_position(T val);
	void push_back(T val);
	bool push_at_position(int pos);
	bool del_at_value(T val);
	bool del_at_position(int pos);
	void show();

	class Iterator 
	{
		Node *iter;
	public:
		Iterator(Node *iter)
		{
			this->iter = iter;
		}
		T &operator *() 
		{
			return iter->val;
		}
		Iterator &operator ++(int)
		{
			Iterator it = *this;
			iter = iter->next;
			return it;
		}
		Iterator &operator --(int)
		{
			Iterator it = *this;
			iter = iter->prev;
			return it;
		}
		bool operator ==(const Iterator& iter2)
		{
			return this->iter == iter2.iter;
		}
		bool operator !=(const Iterator& iter2)
		{
			return this->iter != iter2.iter;
		}
	};
	Iterator begin()
	{
		return Iterator(head);
	}
	Iterator rbegin()
	{
		return Iterator(tail);
	}
	Iterator end()
	{
		return Iterator(tail);
	}
	Iterator rend()
	{
		return Iterator(head);
	}
};

template <typename T> myList<T>::myList()
{
	size = 0;
	head = tail = NULL;
}
template <typename T> myList<T>::myList(const myList &list)
{

}
template <typename T> myList<T>::~myList()
{
	while (size != 0)
	{
		Node *tmp = head->next;
		delete head;
		head = tmp;
		size--;
	}
}
template <typename T> int myList<T>::get_size()
{
	return size;
}
template <typename T> void myList<T>::clear()
{

}
template <typename T> bool myList<T>::is_empty()
{

}
template <typename T> bool myList<T>::value_is_find(T val)
{

}
template <typename T> T& myList<T>::at(int pos)
{

}
template <typename T> bool myList<T>::change_value(int pos)
{

}
template <typename T> int myList<T>::get_position(T val)
{

}
template <typename T> void myList<T>::push_back(T val)
{
	Node *tmp = new Node;
	tmp->val = val;
	if (!head)
	{
		head = tail = tmp;
		tail->next = tmp;
		tail->prev = tmp;
	}
	else
	{
		tmp->next = tail->next;
		tmp->next->prev = tmp;
		tail->next = tmp;
		tmp->prev = tail;
	}
	tail = tmp;
	size++;
}
template <typename T> bool myList<T>::push_at_position(int pos)
{

}
template <typename T> bool myList<T>::del_at_value(T val)
{

}
template <typename T> bool myList<T>::del_at_position(int pos)
{

}
template <typename T> void myList<T>::show()
{
	Node *tmp = head;
	for (int i = 0; i < size; i++)
	{
		cout << tmp->val << endl;
		tmp = tmp->next;
	}
}